from selene.api import *
from pages.LoginPage import LoginPage
import pytest


@pytest.fixture(scope='function')
def open_login_page(open_browser):
    LoginPage().open()
    yield
    browser.quit()


class TestLogin:

    def test_valid_login(self, open_login_page):
        LoginPage().login('s.ivanov@mobidev.biz', 'dispatcher')
        LoginPage().header.should(have.text('Sign Out'))

    def test_empty_login(self, open_login_page):
        LoginPage().login('', '')
        LoginPage().header.should(have.exact_text('Dashboard (Super Dispatch)'))

    def test_incorrect_login(self, open_login_page):
        LoginPage().login('wrong_email', 'wrong_password')
        LoginPage().login_alert.should(have.exact_text('Please enter a correct email and password. '
                                                       'Note that both fields may be case-sensitive.'))


