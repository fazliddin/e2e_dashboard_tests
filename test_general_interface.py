from selene.api import *
from pages.LoginPage import LoginPage
from pages.OrdersPage import OrdersPage
from pages.BasePage import BasePage
from pages.DriversPage import DriversPage
from pages.ReportsPage import ReportsPage
from pages.SettingsPage import SettingsPage
from pages.EditProfilePage import EditProfilePage

# the comment for testing repo
class TestGeneralFunctionality:

    def test_open_drivers(self, login):
        BasePage().open_drivers_page()
        DriversPage().add_driver_button.should(have.exact_text('Add Driver'))

    def test_open_reports(self, login):
        BasePage().open_reports_page()
        ReportsPage().export_header.should(have.exact_text('Export'))

    def test_open_settings(self, login):
        BasePage().open_settings_page()
        SettingsPage().settings_header.should(have.exact_text('Settings'))

    def test_open_edit_profile(self, login):
        BasePage().open_edit_profile()
        EditProfilePage().edit_profile_header.should(have.exact_text('Edit Profile'))

    def test_open_orders(self, login):
        BasePage().open_orders_page()
        OrdersPage().create_order_button.should(have.exact_text('Create Order'))

    def test_sign_out(self, login):
        BasePage().sign_out()
        LoginPage().header.should(have.exact_text('Dashboard (Super Dispatch)'))
