from selene.api import *


class BasePage:
    def __init__(self):
        self.header = s("body > nav > div")
        self.load_offers_header = s('#modal__load-offers > div > div > div.modal-header > h4')
        self.edit_profile = s('#bs-example-navbar-collapse-1 > ul.nav.navbar-nav.navbar-right > li:nth-child(2) > a')

    def open_orders_page(self):
        s('#bs-example-navbar-collapse-1 > ul:nth-child(1) > li:nth-child(1) > a').click()

    def open_contacts_page(self):
        s('#bs-example-navbar-collapse-1 > ul:nth-child(1) > li:nth-child(4) > a').click()

    def open_drivers_page(self):
        s('#bs-example-navbar-collapse-1 > ul:nth-child(1) > li:nth-child(5) > a').click()

    def open_reports_page(self):
        s('#bs-example-navbar-collapse-1 > ul:nth-child(1) > li:nth-child(6) > a').click()

    def open_settings_page(self):
        s('#bs-example-navbar-collapse-1 > ul:nth-child(1) > li:nth-child(7) > a').click()

    def open_load_offers(self):
        s('#load-offers-link').click()

    def open_edit_profile(self):
        self.edit_profile.click()

    def sign_out(self):
        s('#bs-example-navbar-collapse-1 > ul.nav.navbar-nav.navbar-right > li:nth-child(3) > a').click()

    def close_load_offers(self):
        s('#modal__load-offers > div > div > div.modal-header > button').click()

    def unarchive_order(self, order_id):
        browser.open_url('orders/'+order_id+'/unarchive/')

