from selene.api import *
from pages.BasePage import BasePage
from selene.browser import driver
from selenium.common.exceptions import TimeoutException
import time


class OrdersPage(BasePage):
    def __init__(self):
        BasePage.__init__(self)
        self.create_order_button = s('#create-order-button')

    def open(self):
        browser.open_url('orders/')

    def open_n_create(self):
        browser.open_url('orders/create/')

    def create_order(self):
        self.create_order_button.click()

    def search_order(self, order):
        s('#q').send_keys(order)
        s('body > div.content > div > div.row.toolbar > div > div.search-wrap > form > button').click()

    def edit_order(self, order_id):
        s('#order-'+order_id+' > div > div.col-md-3 > a').should(be.clickable).click()

    def go_to_picked_up(self):
        s('#orders-filter > li:nth-child(3) > a').should(be.clickable).click()

    def go_to_delivered(self):
        s('#orders-filter > li:nth-child(4) > a').should(be.clickable).click()

    def go_to_billed(self):
        s('#orders-filter > li:nth-child(5) > a').should(be.clickable).click()

    def go_to_paid(self):
        s('#orders-filter > li:nth-child(6) > a').should(be.clickable).click()

    def go_to_archived(self):
        s('#orders-filter > li:nth-child(7) > a').should(be.clickable).click()

    def go_to_deleted(self):
        s('#orders-filter > li:nth-child(8) > a').should(be.enabled).click()

    def click_load_id_of_pickeup_order(self, order_id):
        s('#order-' + order_id + ' > div > div.col-md-9 > ul.order-info.list-unstyled.list-inline '
                               '> li:nth-child(1) > div > a').click()

    def options_button(self, order_id):
        return s('#order-' + order_id + ' > div > div.col-md-3 > div:nth-child(3) > button')

    def send_invoice(self, order_id, email):
        self.options_button(order_id).should(be.clickable).click()
        s('#order-' + order_id + ' > div > div.col-md-3 > div.dropdown.open > ul > li:nth-child(2) > a').click()
        s('#invoice-email').set_value(email)
        s('#send-invoice-btn').click()
        time.sleep(2)  # todo remove sleep
        driver().switch_to.alert.accept()
        s('body > div.modal-backdrop.fade').should_not(be.enabled)

    def edit_billed_orders(self, order_id):  # it takes time to order to appear in billed
        while True:
            try:
                s('#order-' + order_id + ' > div > div.col-md-3 > a').should(be.visible).click()
                break
            except TimeoutException:
                driver().refresh()

    def unarchive(self, order_id):
        self.options_button(order_id).should(be.clickable).click()
        s('#order-' + order_id + ' > div > div.col-md-3 > div.dropdown.open > ul > li:nth-child(8) > '
                                 'a:nth-child(1)').click()

    def mark_as_payed(self, order_id, payed_ammount, payment_method):
        self.options_button(order_id).should(be.clickable).click()
        s('#order-' + order_id + ' > div > div.col-md-3 > div.dropdown.open > ul > li:nth-child(3) > a').click()
        s('#id_paid_amount').should(be.visible).clear().set_value(payed_ammount)
        s('#id_paid_method').send_keys(payment_method)
        s('#mark_paid_save_btn').should(be.clickable).click()
        time.sleep(2)  # todo remove sleep
        driver().switch_to.alert.accept()
        s('body > div.modal-backdrop.fade').should_not(be.enabled)

    def delete_order(self, order_id):
        self.options_button(order_id).should(be.clickable).click()
        s('#order-' + order_id + ' > div > div.col-md-3 > div.dropdown.open > '
                                 'ul > li:nth-child(6) > a.trash-order-button').click()
        s('#delete-order-btn').should(be.clickable).click()
        s('body > div.modal-backdrop.fade').should_not(be.enabled)

    def archive_order(self, order_id):
        self.options_button(order_id).should(be.clickable).click()
        s('#order-' + order_id + '> div > div.col-md-3 > div.dropdown.open > ul > li:nth-child(6) '
                                 '> a.archive-order-button').click()
