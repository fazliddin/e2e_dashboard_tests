from selene.api import *
from pages.BasePage import BasePage


class ViewOrderPage(BasePage):
    def __init__(self):
        BasePage.__init__(self)
        self.load_id = s('body > div.content > div > div.row.order-view > div.col-md-8.order-view__main > '
                         'div:nth-child(2) > div:nth-child(1) > p:nth-child(1)')
        self.assigned_to = s('body > div.content > div > div.row.order-view > div.col-md-4.order-view__aside > '
                             'div:nth-child(2) > div')
        self.invoice_is_sent = s('body > div.content > div > div.row.order-view > div.col-md-4.order-view__aside > '
                                'div:nth-child(2) > div > ul > li.order-informations > div')
        self.invoice_is_not_sent = s(by.text('Invoice could not be sent'))
        self.payment_is_received = s('body > div.content > div > div.row.order-view > div.col-md-4.order-view__aside '
                                     '> div:nth-child(2) > div > ul > li:nth-child(2) > span')
        self.first_vin = s('body > div.content > div > div.row.order-view > div.col-md-8.order-view__main > '
                           'div:nth-child(3) > div > table > tbody > tr:nth-child(1) > td:nth-child(2)')
        self.first_year = s('body > div.content > div > div.row.order-view > div.col-md-8.order-view__main > '
                            'div:nth-child(3) > div > table > tbody > tr:nth-child(1) > td:nth-child(3)')
        self.first_make = s('body > div.content > div > div.row.order-view > div.col-md-8.order-view__main > '
                            'div:nth-child(3) > div > table > tbody > tr:nth-child(1) > td:nth-child(4)')
        self.first_model = s('body > div.content > div > div.row.order-view > div.col-md-8.order-view__main > '
                             'div:nth-child(3) > div > table > tbody > tr:nth-child(1) > td:nth-child(5)')
        self.first_type = s('body > div.content > div > div.row.order-view > div.col-md-8.order-view__main > '
                            'div:nth-child(3) > div > table > tbody > tr:nth-child(1) > td:nth-child(6)')

        self.second_vin = s(
            'body > div.content > div > div.row.order-view > div.col-md-8.order-view__main > '
            'div:nth-child(3) > div > table > tbody > tr:nth-child(2) > td:nth-child(2)')
        self.second_year = s(
            'body > div.content > div > div.row.order-view > div.col-md-8.order-view__main > '
            'div:nth-child(3) > div > table > tbody > tr:nth-child(2) > td:nth-child(3)')
        self.second_make = s(
            'body > div.content > div > div.row.order-view > div.col-md-8.order-view__main > '
            'div:nth-child(3) > div > table > tbody > tr:nth-child(2) > td:nth-child(4)')
        self.second_model = s(
            'body > div.content > div > div.row.order-view > div.col-md-8.order-view__main > '
            'div:nth-child(3) > div > table > tbody > tr:nth-child(2) > td:nth-child(5)')
        self.second_type = s('body > div.content > div > div.row.order-view > div.col-md-8.order-view__main > '
                             'div:nth-child(3) > div > table > tbody > tr:nth-child(2) > td:nth-child(6)')
        self.first_inop_label = s('body > div.content > div > div.row.order-view > div.col-md-8.order-view__main > '
                                  'div:nth-child(3) > div > table > tbody > tr:nth-child(1) > td:nth-child(10) > span')
        self.origin_address = s('body > div.content > div > div.row.order-view > div.col-md-8.order-view__main > '
                                'div:nth-child(4) > div:nth-child(1) > div > ul > li:nth-child(1)')
        self.destination_address = s('body > div.content > div > div.row.order-view > div.col-md-8.order-view__main > '
                                     'div:nth-child(4) > div:nth-child(2) > div > ul > li:nth-child(1)')
        self.shipper = s('body > div.content > div > div.row.order-view > div.col-md-8.order-view__main > '
                         'div:nth-child(5) > div:nth-child(2) > div > ul > li:nth-child(1)')
        self.price = s('body > div.content > div > div.row.order-view > div.col-md-8.order-view__main > '
                       'div:nth-child(5) > div:nth-child(1) > div > ul > li:nth-child(1) > strong:nth-child(1)')
        self.payment_terms = s('body > div.content > div > div.row.order-view > div.col-md-8.order-view__main > '
                               'div:nth-child(5) > div:nth-child(1) > div > ul > li:nth-child(1) > strong:nth-child(3)')
        self.payed_amount = s('body > div.content > div > div.row.order-view > div.col-md-8.order-view__main > '
                              'div:nth-child(5) > div:nth-child(1) > div > ul > li:nth-child(2) > strong:nth-child(1)')
        self.payment_method = s('body > div.content > div > div.row.order-view > div.col-md-8.order-view__main > '
                                'div:nth-child(5) > div:nth-child(1) > div > ul > li:nth-child(2) > '
                                'trong:nth-child(3)')

    def open(self, order_id):
        browser.open_url('orders/' + order_id + '/view')

    def edit_order(self):
        s('body > div.content > div > div.row.order-view > div.col-md-4.order-view__aside > '
          'div:nth-child(1) > div.col-md-9 > a').click()


