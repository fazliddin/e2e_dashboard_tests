from selene.api import *
from pages.BasePage import BasePage


class LoginPage(BasePage):
    def __init__(self):
        BasePage.__init__(self)
        self.login_alert = s('body > div.container > div > div.col-md-4 > form > div.alert.alert-block.alert-danger')

    def open(self):
        browser.open_url('')

    def login(self, email, password):
        s('#id_username').set_value(email)
        s('#id_password').set_value(password)
        s('body > div.container > div > div.col-md-4 > form > button').click()
