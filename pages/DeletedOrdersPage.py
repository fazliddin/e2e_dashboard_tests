from selene.api import *
from pages.BasePage import BasePage


class DeletedOrdersPage(BasePage):
    def __init__(self):
        BasePage.__init__(self)
        self.first_deleted_order = s('body > app-root > main > app-orders > main > section > app-orders-table >'
                                     ' ngx-datatable > div > datatable-body > datatable-selection > datatable-scroller '
                                     '> datatable-row-wrapper:nth-child(1) > datatable-body-row '
                                     '> div.datatable-row-center.datatable-row-group '
                                     '> datatable-body-cell:nth-child(2)')
        self.clear_selection_button = s('body > app-root > main > app-orders > app-orders-header > '
                                        'header > button.app-link')
        self.deleted_header = s('body > app-root > main > app-orders > app-orders-header > header > h2 > b')
        self.green_banner = s('body > app-root > app-toast > main > div')

    def select_first_deleted_order(self):
        s('body > app-root > main > app-orders > main > section > app-orders-table > ngx-datatable > div > '
          'datatable-body > datatable-selection > datatable-scroller > datatable-row-wrapper:nth-child(1) > '
          'datatable-body-row > div.datatable-row-center.datatable-row-group > '
          'datatable-body-cell:nth-child(1) > div > label > input[type="checkbox"]').click()

    def clear_selection(self):
        self.clear_selection_button.click()

    def restore_order(self):
        s('body > app-root > main > app-orders > app-orders-header > header > button.app-button').click()

    def back_to_orders(self):
        s('body > app-root > main > app-orders > app-orders-header > header > a').click()