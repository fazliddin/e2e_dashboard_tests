# README #


### What is this repository for? ###

* Demo of e2e autotests for SD Dashboard
* Technology stack: python (3.5 preferable), pytest (https://docs.pytest.org/en/latest/), selene (https://github.com/yashaka/selene), allure (http://allure.qatools.ru/)


### How to? ###

1. Setup python environment using requirments.txt from repo.
It's better to use virtual environment (http://python-guide-pt-br.readthedocs.io/en/latest/dev/virtualenvs/), but not necessary. 

cd to the directory where requirements.txt is located
run: pip install -r requirements.txt in your shell

2. To run tests, use
python -m pytest

3. To run tests with report use
python -m pytest --alluredir ./reports/

4. To open repors use
allure generate ./reports/
allure report open